#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "radio.h"

#define NRF_ESB_MAX_PAYLOAD_LENGTH 252
#define NRF_ESB_PIPE_COUNT 2

#include "sdk_common.h"
#include "nrf.h"
#include "nrf_esb.h"
#include "nrf_error.h"
#include "nrf_esb_error_codes.h"
#include "app_error.h"

static void nrf_esb_event_handler(nrf_esb_evt_t const * p_event);


static RadioDataReceivedCallback receivedCb = NULL;
static RadioDataSentCallback sentCb = NULL;
static bool tx_busy = false;

static uint8_t base_addr_0[ADDRESS_LENGTH - 1] = {0xE7, 0xE7, 0xE7, 0xE7};
static uint8_t base_addr_1[ADDRESS_LENGTH - 1] = {0xC2, 0xC2, 0xC2, 0xC2};
static uint8_t addr_prefix[NRF_ESB_PIPE_COUNT] = {0xE7, 0xC2};

void radioInit(RadioDataReceivedCallback rxCb, RadioDataSentCallback txCb)
{
    receivedCb = rxCb;
    sentCb = txCb;

    uint32_t err_code;

    nrf_esb_config_t nrf_esb_config         = NRF_ESB_DEFAULT_CONFIG;
    nrf_esb_config.protocol                 = NRF_ESB_PROTOCOL_ESB_DPL;
    nrf_esb_config.retransmit_delay         = 600;
    nrf_esb_config.bitrate                  = NRF_ESB_BITRATE_2MBPS;
    nrf_esb_config.event_handler            = nrf_esb_event_handler;
    nrf_esb_config.mode                     = NRF_ESB_MODE_PTX;
    nrf_esb_config.selective_auto_ack       = false;
    nrf_esb_config.tx_mode                  = NRF_ESB_TXMODE_MANUAL;

    err_code = nrf_esb_init(&nrf_esb_config);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_esb_set_address_length(ADDRESS_LENGTH - 1);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_esb_set_base_address_0(base_addr_0);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_esb_set_base_address_1(base_addr_1);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_esb_set_prefixes(addr_prefix, NRF_ESB_PIPE_COUNT);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_esb_set_address_length(ADDRESS_LENGTH - 1);
    APP_ERROR_CHECK(err_code);
}

void radioSendData(const uint8_t *data, size_t size)
{
    if (size > NRF_ESB_MAX_PAYLOAD_LENGTH) {
        return;
    }

    nrf_esb_payload_t tx_payload = {
        .length = size,
        .pipe = 1,
        .noack = false,
    };

    memcpy(tx_payload.data, data, size);
    if (nrf_esb_write_payload(&tx_payload) == NRF_SUCCESS) {
        tx_busy = true;
        if (nrf_esb_start_tx() == NRF_SUCCESS) {
            while (tx_busy) {}
        } else {
            tx_busy = false;
        }
    }

}

void radioSetChannel(uint8_t ch)
{
    uint32_t err_code = nrf_esb_set_rf_channel(ch);
    APP_ERROR_CHECK(err_code);
}

void radioSetOwnAddress(const uint8_t address[ADDRESS_LENGTH])
{
    memcpy(base_addr_0, address, ADDRESS_LENGTH - 1);
    addr_prefix[0] = address[ADDRESS_LENGTH - 1];
    // set first ADDRESS_LENGTH - 1 bytes
    uint32_t err_code = nrf_esb_set_base_address_0(base_addr_0);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_esb_set_prefixes(addr_prefix, NRF_ESB_PIPE_COUNT);
    APP_ERROR_CHECK(err_code);
}

void radioSetTxAddress(const uint8_t address[ADDRESS_LENGTH])
{
    memcpy(base_addr_1, address, ADDRESS_LENGTH - 1);
    addr_prefix[1] = address[ADDRESS_LENGTH - 1];
    // set first ADDRESS_LENGTH - 1 bytes
    uint32_t err_code = nrf_esb_set_base_address_1(base_addr_1);
    APP_ERROR_CHECK(err_code);
    err_code = nrf_esb_set_prefixes(addr_prefix, NRF_ESB_PIPE_COUNT);
    APP_ERROR_CHECK(err_code);
}

void radioSetMode(RadioMode mode)
{
    switch (mode) {
    case RADIO_MODE_TX:
        break;
    case RADIO_MODE_RX:
        nrf_esb_start_rx();
        break;
    }
}

static void nrf_esb_event_handler(nrf_esb_evt_t const * p_event)
{
    switch (p_event->evt_id)
    {
        case NRF_ESB_EVENT_TX_SUCCESS:
            //NRF_LOG_DEBUG("TX SUCCESS EVENT");
            tx_busy = false;
            break;
        case NRF_ESB_EVENT_TX_FAILED:
            //NRF_LOG_DEBUG("TX FAILED EVENT");
            (void) nrf_esb_flush_tx();
            (void) nrf_esb_start_tx();
            tx_busy = false;
            break;
        case NRF_ESB_EVENT_RX_RECEIVED:
            //NRF_LOG_DEBUG("RX RECEIVED EVENT");
            break;
    }
}
