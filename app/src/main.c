#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "nrf_delay.h"
#include "app_timer.h"
#include "nrfx_clock.h"
#include "nrf_pwr_mgmt.h"
#include "nrf_drv_clock.h"
#include "radio.h"

static volatile bool lfClkStarted = false;
static bool sendFrame = false;

APP_TIMER_DEF(mouseTimer);


static void clockEvtHandler(nrfx_clock_evt_type_t event);
static void timerTimeoutHandler(void *ctx);
void clocks_start(void);

static uint8_t ledData[3 * 8] = {0};

int main(void)
{
    // nrf_clock required for usb
    ret_code_t ret = nrf_drv_clock_init();
    APP_ERROR_CHECK(ret);

    // lfclk required for app_timer
    nrf_drv_clock_lfclk_request(NULL);
    while (!nrf_drv_clock_lfclk_is_running()) {}
    ret = app_timer_init();
    ret = app_timer_create(&mouseTimer, APP_TIMER_MODE_REPEATED, timerTimeoutHandler);
    ret = app_timer_start(mouseTimer, APP_TIMER_TICKS(25), NULL);
    clocks_start();

    radioInit(NULL, NULL);
    radioSetChannel(32);

    static const uint8_t addrTx[] = {0x67, 0x33, 0x56, 0xFB, 0x0C};
    static const uint8_t addrRx[] = {0x67, 0x33, 0x56, 0xFB, 0x0E};
    radioSetTxAddress(addrTx);
    radioSetOwnAddress(addrRx);

    while (true)
    {
        if (sendFrame) {
            sendFrame = false;
            //static const uint8_t data[] = {0x00, 0xFF, 0xFF};
            static uint32_t idx = 0;
            memset(ledData, 0, sizeof(ledData));
            for (uint32_t i = 0; i < sizeof(ledData) / 3; i++) {
                if (idx == i) {
                    ledData[i * 3] = 0xFF;
                    ledData[i * 3 + 1] = 0xFF;
                    ledData[i * 3 + 2] = 0xFF;
                }
            }
            radioSendData(ledData, sizeof(ledData));
            idx++;
            if (idx >= 8) {
                idx = 0;
            }


            nrf_delay_ms(1000);
        }
    }
}

static void timerTimeoutHandler(void *ctx)
{
    (void)ctx;
    sendFrame = true;
}

static void clockEvtHandler(nrfx_clock_evt_type_t event)
{
    switch(event) {
        case NRFX_CLOCK_EVT_LFCLK_STARTED:
            lfClkStarted = true;
            break;
    }
}

void clocks_start( void )
{
    NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
    NRF_CLOCK->TASKS_HFCLKSTART = 1;

    while (NRF_CLOCK->EVENTS_HFCLKSTARTED == 0);
}
