#pragma once

#include <stdint.h>
#include <stddef.h>

#define ADDRESS_LENGTH (5U)

typedef enum {
    RADIO_MODE_TX,
    RADIO_MODE_RX,
    RADIO_MODE_COUNT
} RadioMode;

typedef void (*RadioDataReceivedCallback)(const uint8_t *data, size_t size);
typedef void (*RadioDataSentCallback)(void);

void radioInit(RadioDataReceivedCallback rxCb, RadioDataSentCallback txCb);
void radioSendData(const uint8_t *data, size_t size);
void radioSetChannel(uint8_t ch);
void radioSetOwnAddress(const uint8_t address[ADDRESS_LENGTH]);
void radioSetTxAddress(const uint8_t address[ADDRESS_LENGTH]);
void radioSetMode(RadioMode mode);
